function getAccessToLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {}, errorPosition);
    } else {
       alert("Geolocation is not supported by this browser.");
    }
}

function getCurrentLocation(barcode) {
    $.LoadingOverlay("show");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            showPosition(position, barcode);
        }, errorPosition);
    } else {
       $.LoadingOverlay("hide", true);
       alert("Geolocation is not supported by this browser.");
    }
}

function watchLocation(barcode) {
    
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(function(position) {
            showPosition(position, barcode);
        }, errorPosition);
    } else {
       alert("Geolocation is not supported by this browser.");
    }
}

function readNTimes(quantity){
    $.LoadingOverlay("show");
    if(quantity > 0){
        setTimeout(function(){
            getCurrentLocation();
            readNTimes(quantity-1);
        }, 1000);
    }
}

function showPosition(position, barcode) {
    $('#latitude').val(position.coords.latitude);
    $('#longitude').val(position.coords.longitude);
    
    var positionObject = { 'datetime': calcDatetime(), 'barcode': barcode, 'lat' : position.coords.latitude, 'long' : position.coords.longitude };
    readings[readings.length] = positionObject;
    setReadingsToLocalStorage();
    addHistory(positionObject);
    if(barcode !== undefined){
        Quagga.start();
        QuaggaApp.lastResult = null;
    }
    $.LoadingOverlay("hide", true);
   
}

function errorPosition(error) {
    alert('Error al obtener posición,por favor active el GPS y reinicie la aplicación.');
    
}