function clearReadingsFromLocalStorage(){
    $('#history').empty();
    readings = [];
    $.each(localStorage, function(key, value) {
        localStorage.removeItem(key);
    });
    
}

function loadReadingsFromLocalStorage(){
    
    var storagePositions = JSON.parse(localStorage.getItem('readings'));
    if (storagePositions !== null) {
        readings = storagePositions;
    }
    
    $.each(readings, function(key, positionObject) {
        addHistory(positionObject);
    });
}
         
function setReadingsToLocalStorage(){
    localStorage.setItem('readings', JSON.stringify(readings));
}