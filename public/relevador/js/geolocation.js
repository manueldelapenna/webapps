var Geolocator = {};
Geolocator.getAccessToLocation=function(geoOptions) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {}, errorPosition, geoOptions);
    } else {
       alert("Geolocation is not supported by this browser.");
    }
}
Geolocator.getCurrentLocation=function(callbackFunction, secuencia, geoOptions) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            if(callbackFunction){
                callbackFunction(position, secuencia);
            }
        }, errorPosition, geoOptions);
    } else {
       alert("Geolocation is not supported by this browser.");
    }
}
Geolocator.watchLocation=function(callbackFunction, geoOptions) {
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(function(position) {
            if(callbackFunction){
                callbackFunction(position);
            }
        }, errorPosition, geoOptions);
    } else {
       alert("Geolocation is not supported by this browser.");
    }
}
function errorPosition(error) {
    alert('Error al obtener posición,por favor active el GPS y reinicie la aplicación.');
    
}