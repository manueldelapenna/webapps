/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Cache names

var dataCacheName = 'PositionData'

var cacheName = 'Position'

// Application shell files to be cached

var filesToCache = [

    "js/position-logger.js",
    "js/local-storage.js",
    "js/geolocation.js",
    "js/functions.js",
    "lib/quaggaJS/example/vendor/jquery-1.9.0.min.js",
    "lib/offlineJS/js/offline.min.js",
    "lib/offlineJS/css/offline-language-spanish-indicator.css", 
    "lib/offlineJS/css/offline-theme-chrome-indicator.css",
    "lib/offlineJS/css/offline-language-spanish.css"
]

self.addEventListener('install', function (e) {

    console.log('[ServiceWorker] Install')

    e.waitUntil(
            caches.open(cacheName).then(function (cache) {

        console.log('[ServiceWorker] Caching app shell')

        return cache.addAll(filesToCache)

    })

            )

})

self.addEventListener('activate', function (e) {

    console.log('[ServiceWorker] Activate')

    e.waitUntil(
            caches.keys().then(function (keyList) {

        return Promise.all(keyList.map(function (key) {

            if (key !== cacheName && key !== dataCacheName) {

                console.log('[ServiceWorker] Removing old cache', key)

                return caches.delete(key)

            }

        }))

    })

            )

    return self.clients.claim()

})

self.addEventListener('fetch', function (e) {

    console.log('[ServiceWorker] Fetch', e.request.url)

    e.respondWith(
            caches.match(e.request).then(function (response) {

        return response || fetch(e.request)

    })

            )

})
